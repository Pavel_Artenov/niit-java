package Staff;
import org.w3c.dom.*;

import java.lang.*;
import javax.xml.parsers.*;
import java.io.File;

public class App 
{
    public static void main( String[] args ) {
        Employee []empArr = new Employee[30];
        try{
            String file = "Staff.xml";
            File xmlFile = new File(file);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);                        //создали дерево
            NodeList empList = doc.getElementsByTagName("employee");                 //создали список из работников
            for(int i = 0;i < empList.getLength();i++){
                Node node = empList.item(i);
                Element element = (Element) node;
                String temp = element.getAttribute("position");
                switch (temp){
                    case "Cleaner" :
                        empArr[i] = new Cleaner(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                "Cleaner");
                        break;
                    case "Driver" :
                        empArr[i] = new Driver(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                "Driver");
                        break;
                    case "Programmer" :
                        empArr[i] = new Programmer(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("rate").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("budgetproject").item(0).getTextContent()),
                                Double.parseDouble(element.getElementsByTagName("coefficientofparticipation").item(0).getTextContent()),
                                "Programmer");
                        break;
                    case "Tester" :
                        empArr[i] = new Tester(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("rate").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("budgetproject").item(0).getTextContent()),
                                Double.parseDouble(element.getElementsByTagName("coefficientofparticipation").item(0).getTextContent()),
                                "Tester");
                        break;
                    case "Teamleader" :
                        empArr[i] = new TeamLeader(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("rate").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("budgetproject").item(0).getTextContent()),
                                Double.parseDouble(element.getElementsByTagName("coefficientofparticipation").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("countsubordinate").item(0).getTextContent()),
                                "TeamLeader");
                        break;
                    case "Manager" :
                        empArr[i] = new Manager(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                Double.parseDouble(element.getElementsByTagName("coefficientofparticipation").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("budgetproject").item(0).getTextContent()),
                                "Manager");
                        break;
                    case "Projectmanager" :
                        empArr[i] = new ProjectManager(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                Double.parseDouble(element.getElementsByTagName("coefficientofparticipation").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("budgetproject").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("countsubordinate").item(0).getTextContent()),
                                "ProjectManager");
                        break;
                    case "Seniormanager" :
                        empArr[i] = new SeniorManager(Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent()),
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                Integer.parseInt(element.getElementsByTagName("worktime").item(0).getTextContent()),
                                Double.parseDouble(element.getElementsByTagName("coefficientofparticipation").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("budgetproject").item(0).getTextContent()),
                                Integer.parseInt(element.getElementsByTagName("countsubordinate").item(0).getTextContent()),
                                "SeniorManager");
                }
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        int i = 0;
        while(empArr[i]!=null){
            System.out.println(empArr[i].getId()+"\t"+empArr[i].getPosition()+"\t"+ empArr[i].paymentCalc());
            i++;
        }
    }
}
