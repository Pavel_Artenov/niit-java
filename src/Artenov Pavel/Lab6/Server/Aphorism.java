package Server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by paul on 23.06.17.
 */
public class Aphorism extends Thread {
    private PrintWriter printWriter;
    private String [] arr = new String[30];


    public Aphorism(PrintWriter printWriter){
        this.printWriter = printWriter;
        start();
    }

    public void run(){

        read();
        getAphorism();

    }

    public void read(){
        String temp;
        int i = 0;
        try {

            File file = new File("Aphorism");

            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            while ((temp = br.readLine())!=null){
                arr[i] = temp;
                i++;
            }

            }

        catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public void getAphorism(){
        String aphorism;
        try {
            while (true) {
                printWriter.print("Aphorism*" + getRandAphorism());
                sleep(20000);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

    }
    public String getRandAphorism(){
        int i;
        i = (int) (Math.random() * 20);
        return arr[i];
    }
}
