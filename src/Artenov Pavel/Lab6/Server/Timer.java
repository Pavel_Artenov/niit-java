package Server;

import java.io.PrintWriter;
import java.util.Calendar;

/**
 * Created by paul on 23.06.17.
 */
public class Timer extends Thread {

    private PrintWriter printWriter;
    public Timer(){};

    public Timer(PrintWriter printWriter){
        this.printWriter = printWriter;
        start();
    }
    public void run(){
       getTime();
    }

    public void getTime(){
        String str;
        try {
            while (true){
                Calendar now = Calendar.getInstance();
                int hour = now.get(Calendar.HOUR_OF_DAY);
                int min = now.get(Calendar.MINUTE);
                int sec = now.get(Calendar.SECOND);
                str = "Timer*"+hour+":"+min+":"+sec;
                printWriter.println("Timer*"+hour+":"+min+":"+sec);
                sleep(800);

            }
        }catch (Exception ex){
            ex.printStackTrace();

        }


    }

}
